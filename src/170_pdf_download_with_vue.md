# download pdf

## ex:1


template
```
<a href="#" @click="submitPost('pdf')" >   PDF download  </a>
```

script
```
submitPost(e) {
 axios({
  url: '/download',
  method: 'GET',
  responseType: 'blob', // important
 }).then((response) => {
  const url = window.URL.createObjectURL(new Blob([response.data], { type: 'application/pdf' }));
  const link = document.createElement('a');
  link.href = url;
  link.target = '_blank'
  link.click();
 });
}
```

routes/web.php
```
Route::get('/download', function (Request $request) {
			return Storage::download('yevmiye.pdf'); // ok. laravel default file inside "/storage/app/"
 });
```