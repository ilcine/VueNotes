https://medium.com/js-dojo/component-communication-in-vue-js-ca8b591d7efa

# 1) child den parent e 

## 	for child 

```
<script>
export default {
	name: 'Child',
	
	methods: {
        sendMessageToParent: function () {
         this.$emit('messageFromChild', 'someValue')   
        }
</script>        
```

## for parent

```
<script>
import Child from './Child.vue'
export default {
	name: 'Parent',
	components: {
		Child
	},

	
	methods: {
	    fromChild (value) {
			console.log('geldimi' + value); // someValue
    }
</script>       
<template>
	<div>	
		<component  @messageFromChild="fromChild" ></component>
	</div>
</template>
```

# 2) parent ten child e

## for parent

```
<script>
import Child from './Child.vue'
export default {
	name: 'Parent',
	data() {
	    return {
	        message: 'my client message'
	    }
	}
	components: {
		Child
	},
</script>       
<template>
	<div>	
		<component  :message="child" ></component>
	</div>
</template>
```

## for child

```
<script>
export default {
	name: 'Child',
	props: {
	    message: String
	}
}	
	 
</script> 

<template>
	<div>	
		{{ message  }}
	</div>
</template>

```

# 3) Dynamic components and props.


## Menu side

```
<template>
<div>
 <div>
  <a href="#" @click="showWhich = 'DynamicOne'"> DynamicOne </a>
  <a href="#" @click="showWhich = 'DynamicTwo'"> DynamicTwo </a>
 </div>
    
 <div>
   <component :is="showWhich"  :emr="123"></component>
 </div>
</div>
</template>

<script>

import DynamicOne from './../test/DynamicOne.vue'
import DynamicTwo from './../test/DynamicTwo.vue'

export default {
 
 components: {
   DynamicOne, DynamicTwo
 },
	
 data(){
  return {
    showWhich: 'DynamicOne', // default Component
	}
  },
  
}
</script>
```

## DynamicOne page

```
<template>
  <div>Dynamic Component One  {{ emr }}</div>
</template>
<script>
export default {
  props: ['emr'],
  name: 'DynamicOne',
  mounted() { 
	console.log('This props:' + this.emr );
  }
}
</script>
```

## DynamicTwo page

```
<template>
  <div>Dynamic Component Two  {{ emr }}</div>
</template>
<script>
export default {
  props: ['emr'],
  name: 'DynamicTwo',
  mounted() { 
	console.log('This props:' + this.emr );
  }
}
</script>
```






