# new tab for vue

## ex1: for router-link (vue-router)

```
<router-link :to="{path: '/api/MuhYevExport', 
     params: {id: '5', type:'pdf'}, 
     headers: {'Content-Type2': 'multipart/form-data'}
   }" 
   target='_blank' 
   > pdf export 
</router-link> 
```

## ex2: for vue

template
```
<a href="#"  @click="submitPost('pdf')" > Export </a>
```

script
```
let routeData = this.$router.resolve({path: '/api/MuhYevExport', 
	query: {id: '5', type:'pdf'},
	headers: {'Content-Type2': 'multipart/form-data'}
	}); 
window.open(routeData.href, '_blank'); 
```

routes/web.php
```
Route::get   ('/MuhYevExport', 'Muh\MuhYevController@MuhYevExport');
```

controller
```
public function MuhYevExport(Request $request)
  {		
     dd( $request);
     //or
	 return response()->json(['posts' => $request->all(),], 200);
  }
```  