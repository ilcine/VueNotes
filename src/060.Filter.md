# Vue notları
> _Emrullah İLÇİN_
  
### `filter ör1`  // catfilter array dır.

```
<div v-for="post in chatPostsX.filter(post => post.day_at === day_at)">
<p> {{ post.day_at }} </p>
</div>
```

### `filter ör2`

```
<div v-for="post in chatPostsX" v-if="post.day_at === post1.day_at">  
<p> {{ post.day_at }} </p>
</div>
```

### `filter ör3`

```
<div v-for="post in chatFilter('2018-09-22')">
<p> {{ post }} </p>
</div>
```

* methods a 

```
methods: {

chatFilter(day_at){
	var a = null;
	a = this.chatPostsX.filter((post) => {
		return post.day_at.match(day_at)    
	});		
	a.forEach((chatSearch, i) => {
		let a = {};
		a.day_at =chatSearch.day_at;
		a.chatMessages =chatSearch.chatMessages;
	});
	return a;
},

},
```

### js filter ex:1

```
optionsAy: [
	{ id:1, value:'Ocak', checked: true},
	{ id:2, value:'Şubat', checked: true},
	{ id:3, value:'Mart', checked: false},
	{ id:4, value:'Nisan', checked: false},
	{ id:5, value:'Mayıs', checked: false},
	{ id:6, value:'Haziran', checked: false},
	{ id:7, value:'Temmuz', checked: false},
	{ id:8, value:'Ağustos', checked: false},
	{ id:9, value:'Eylül', checked: false},
	{ id:10, value:'Ekim', checked: false},
	{ id:11, value:'Kasım', checked: false},
	{ id:12, value:'Aralık', checked: false},
],
```

```
var va = this.optionsAy.filter(item => item.checked).map(name => name.value); // bu ay adlarını alır
var ay = this.optionsAy.filter(item => item.checked).map(name => name.id); // bu ay sayılarını alır

console.log( 'value : ' + JSON.stringify( va ) );  //  (Ocak,Şubat)
console.log( 'ay id : ' + JSON.stringify( ay ) );  //  (1,2)
```


		
		
		





