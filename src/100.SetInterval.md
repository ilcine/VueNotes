# Vue notları
> _Emrullah İLÇİN_
  
### `setInterval`

* methods a 
```
chatSonsuzAktifStart()
		{
			var app = this; 
			app.readChatPosts();
			app.chatSonsuzAktif = setInterval(app.readChatPosts(), 5000);
			console.log('sonsuz CHAT okuma tetiklendi');
		},	
		
```

* methods a   
```
chatSonsuzAktifStop()
		{
			var app = this;
			clearInterval(app.chatSonsuzAktif);
			console.log("sürekli mesj okuma durdu:" );		 			
		},	
```
* template ye 
```
<button @click="chatSonsuzAktifStart()" class="btn btn-success btn-xs">Başla</button>
<button @click="chatSonsuzAktifStop()" class="btn btn-danger btn-xs">Dur</button>
```

* methods a

```
methods: {
		readChatPosts()
		{
    console_log("dönüyorum");
    }
 }   
```

